#!/user/bin/env python3
#coding=utf-8
# def infoprint ():
#     print ("weixin")
#     print("24")
# infoprint()
# def addnum(a,b):
#     c = a+b
#     print(c)
# addnum(10,9)
# def addnum3(a,b,c):
#     d = a+b-c
#     print(d)
# addnum3(1,2,3)
# def print01():
#     print ("-"*10)
#     return()
# def print02():
#     i = 0
#     while i <4:
#         print01()
#         i+=1
# print02()
# def add3(a,b,c):
#     d=a+b+c
#     print(d)
#     return
# add3(1,2,3)
# def avg3(a,b,c):
#     d=(a+b+c)/3.0
#     print(d)
#     return
# avg3(1,2,3)
# global a
# a = 100
# def jubu():
#     a = 200
#     print(a)
#     return
# jubu()
# def printinfo( name, age = 35 ):
#    # 打印任何传入的字符串
#    print ("Name: ", name)
#    print ("Age ", age)
#
# # 调用printinfo函数
# printinfo(name="miki" )
# printinfo( age=9,name="miki" )
#不定长参数
def fun(a, b, *args, **kwargs):
    """可变参数演示示例"""
    print ("a =", a)
    print ("b =", b)
    print ("args =", args)
    print ("kwargs: ")
    for key, value in kwargs.items():
        print (key, "=", value)
c = (3, 4, 5)
d = {"m":6, "n":7, "p":8}
fun(1, 2, *c, **d)
print('='*20)
fun(1, 2, 3, 4, 5, m=6, n=7, p=8)
#可变类型和不可变类型（指针）
a = 1
b = a
a = 2
print(b)
#id值理解为那块内存的地址标示
print(id(a))
print(id(b))
a = [1,2]
b = a
a.append(3)
print(b)


