#!/usr/bin/env python3
#coding=utf-8
num = int(input("请输入要生成的行数N:"))
i = 1  # 行数
while i <= num:
        if i <= num - i:  # 增
                k = i  # 每行生成的个数
                while k > 0:
                        print("*", end='')
                        k -= 1
                print("\n")
        else:  # 减
                k = num - i + 1  # 生成的个数
                while k > 0:
                        print("*", end='')
                        k -= 1
                print("\n")
        i += 1
