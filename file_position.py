#!/user/bin/env python3
#coding=utf-8
#打开一个已存在的文件
f = open('file.txt','r')
str = f.read(3)
print('读取的数据是：',str)
#获取当前位置f.tell()
position = f.tell()
print('当前位置：',position)
f.close()

# 定位到某个位置seek(offset,from)偏移量，方向：0文件开头1当前位置2文件末尾
# f = open('file.txt','r')
# str = f.read(20)
# f.seek(5,0)
# position = f.tell()
# print ('当前位置：',position)
# f.close()

# 打开一个已经存在的文件
# f = open("file.txt", "r")
# # 查找当前位置
# position = f.tell()
# print ("当前文件位置 : ", position)
# # 重新设置位置
# f.seek(-3,2)
# # 读取到的数据为：文件最后3个字节数据
# str = f.read()
# print ("读取的数据是 : ", str)
# f.close()
#rename file
# import os
# os.rename('file1.txt','file.txt')
# #remove file
# os.remove('毕业论文.txt')
# #mkdir file
# os.mkdir('毕业论文.txt')
# #获取当前目录
# os.getcwd('毕业论文.txt')
# #更改默认目录
# os.chdir('../')
# #获取目录列表
# os.listdir('./')
# #删除文件夹
# os.rmdir('文件夹名')




