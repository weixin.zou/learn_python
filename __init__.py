#!/user/bin/env python3
#coding=utf-8
'''__init__()方法，在创建一个对象时默认被调用，不需要手动调用
__init__(self)中，默认有1个参数名字为self，如果在创建对象时传递了2个实参，
那么__init__(self)中出了self作为第一个形参外还需要2个形参，例如__init__(self,x,y)
__init__(self)中的self参数，不需要开发者传递，python解释器会自动把当前的对象引用传递进去'''
class car :
    def __init__(self):
        self.color = 'red'
        self.size = 'max'
        self.speed = 'fastest'
reallyCar = car()
print(reallyCar.color)
print(reallyCar.speed)
#根据上两节创建一个Car类 创建出多个汽车对象，比如BMW、AUDI等
class Car :
    def __init__(self,newWheelNum,newColor):
        self.WheelNum = newWheelNum
        self.Color = newColor
BMW = Car(4,'green')
AUDI = Car(5,'red')

print(BMW.WheelNum)
print(BMW.Color)
# class dd :
#     def __init__(self):
#         self.ddSize=20
# weixin = dd()
# print(weixin.ddSize,end='cm')
















