#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 作者：zouweixin
# 网址：https://gitlab.com/weixin.zou
'''Python中没有像C++中public和private这些关键字来区别公有属性和私有属性
它是以属性命名方式来区分，如果在属性名前面加了2个下划线'__'，则表明该属性是私有属性，否则为公有属性
（方法也是一样，方法名前面加了2个下划线的话表示该方法是私有的，否则为公有的）。'''
class People(object):

    def __init__(self, name):
        self.__name = name

    def getName(self):
        return self.__name

    def setName(self, newName):
        if len(newName) >= 5:
            self.__name = newName
        else:
            print("error:名字长度需要大于或者等于5")

xiaoming = People("dongGe")
xiaoming.setName('wanger')
print(xiaoming.getName())
xiaoming.setName('lisi')
print(xiaoming.getName())
'''当有1个变量保存了对象的引用时，此对象的引用计数就会加14
当使用del删除变量指向的对象时，如果对象的引用计数不会1，比如3，那么此时只会让这个引用计数减1，即变为2，
当再次调用del时，变为1，如果再调用1次del，此时会真的把对象进行删除'''
import time
class Animal(object):
    # 初始化方法
    # 创建完对象后会自动被调用
    def __init__(self, name):
        print('__init__方法被调用')
        self.__name = name
    # 析构方法
    # 当对象被删除时，会自动被调用
    def __del__(self):
        print("__del__方法被调用")
        print("%s对象马上被干掉了..."%self.__name)
# 创建对象
dog = Animal("哈皮狗")
# 删除对象
del dog
cat = Animal("波斯猫")
cat2 = cat
cat3 = cat
print("---马上 删除cat对象")
del cat
print("---马上 删除cat2对象")
del cat2
print("---马上 删除cat3对象")
del cat3
print("程序2秒钟后结束")
time.sleep(2)



