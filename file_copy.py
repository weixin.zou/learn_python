#!/user/bin/env python3
#coding=utf-8
#提示输入文件
oldFileName = input('请输入需要备份的文件名字：')
#已读方式打开文件
oldFile = open(oldFileName,'rb')
#提取文件后缀
fileFlagNum = oldFileName.rfind('.')
if fileFlagNum > 0:
    fileFlag = oldFileName[fileFlagNum:]
#组织新的文件名字
newFileName = oldFileName[:fileFlagNum] + '[复件]' +fileFlag
#创建新文件
newFile = open(newFileName,'wb')
#把旧文件的数据，一行一行的复制到新文件
for lineContent in oldFile.readlines():
    newFile.write(lineContent)
#关闭文件
oldFile.close()
newFile.close()





