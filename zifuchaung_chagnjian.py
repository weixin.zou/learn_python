#/user/bin/env python3
#coding=utf-8
mystr = 'hello world itcast and itcastcpp'
#find 检测str是否包含在mystr中，如果是就返回开始的索引值，否则返回-1
print(mystr.find('itcast',0,-1))
#index跟find()方法一样，只不过如果str不在 mystr中会报一个异常.
print(mystr.index('itcast',0,-1))
#count返回str在start、end之间mystr里面出现的次数
print(mystr.count('itcast',0,-1))
#replace把 mystr 中的 str1 替换成 str2,如果 count 指定，则替换不超过 count 次
print(mystr.replace('itcast','fuck',mystr.count('itcast')))
#split以 str 为分隔符切片 mystr，如果 maxsplit有指定值，则仅分隔 maxsplit 个子字符串(切maxsplit)
print(mystr.split(' ',2))
#capitalize把字符串的第一个字符大写
print(mystr.capitalize())
#title每个单词首字母大写
print(mystr.title())
#startswith检查字符串是否是以 obj 开头, 是则返回 True，否则返回 False
print(mystr.startswith('obj'))
print(mystr.startswith('hello'))
#endswith检查字符串是否以obj结束，如果是返回True,否则返回 False.
print(mystr.endswith('obj'))
print(mystr.endswith('cpp'))
#lower转换 mystr 中所有大写字符为小写
mystr_title = mystr.title()
print(mystr_title)
print(mystr_title.lower())
#upper转换 mystr 中的小写字母为大写
print(mystr_title.upper())
#返回一个原字符串左对齐,并使用空格填充至长度 width 的新字符串
mystr = 'hello'
print(mystr.ljust(10))
#返回一个原字符串右对齐,并使用空格填充至长度 width 的新字符串
print(mystr.rjust(10))
#返回一个原字符串居中,并使用空格填充至长度 width 的新字符串
print(mystr.center(10))
#删除 mystr 左边的空白字符
mystr = '     1   hello   1    '
print(mystr.lstrip())
#删除 mystr 字符串末尾的空白字符
print(mystr.rstrip())
#strip删除mystr字符串两端的空白字符
print(mystr.strip())
#类似于 find()函数，不过是从右边开始查找.
print(mystr.rfind('hello', 0,-1 ))
#类似于 index()，不过是从右边开始.
print(mystr.rindex('hello', 0,-1 ))
#把mystr以str分割成三部分,str前，str和str后
mystr = 'hello world itcast and itcastcpp'
print(mystr.partition('itcast'))
#类似于 partition()函数,不过是从右边开始.
print(mystr.rpartition('itcast'))
#按照行分隔，返回一个包含各行作为元素的列表
print(mystr.splitlines())
#如果 mystr 所有字符都是字母 则返回 True,否则返回 False
mystr = 'abc'
print(mystr.isalpha())
mystr = 'a c'
print(mystr.isalpha())
#如果 mystr 只包含数字则返回 True 否则返回 False.
mystr = 'a c'
print(mystr.isdigit())
mystr = '123'
print(mystr.isdigit())
#如果 mystr 所有字符都是字母或数字则返回 True,否则返回 False
mystr = '123abc'
print(mystr.isalnum())
mystr = '123 abc'
print(mystr.isalnum())
#如果 mystr 中只包含空格，则返回 True，否则返回 False.
mystr = '123abc'
print(mystr.isspace())
mystr = '   '
print(mystr.isspace())
#mystr 中每个字符后面插入str,构造出一个新的字符串
mystr = '_'
str = ['my','name','is','wei','xin']
print(mystr.join(str))
aStr = 'haha nihao a \t heihei \t woshi nide \t hao \npengyou'
print(aStr.split('\t')[-2])
















