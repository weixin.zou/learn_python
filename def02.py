print('递归函数_阶乘')
def calNum(num):
    i = 1
    result = 1
    while i <= num:
        result *=i
        i+=1
    return result
ret = calNum(0)
print(ret)
#######################
def calNum01(num):
    if num >= 1:
        result = num * calNum01(num-1)
    else:
        result = 1
    return result
ret = calNum01(3)
print(ret)
print('匿名函数')
#Lambda函数能接收任何数量的参数但只能返回一个表达式的值,匿名函数不能直接调用print，因为lambda需要一个表达式

stus = [
    {"name":"zhangsan", "age":18},
    {"name":"lisi", "age":19},
    {"name":"wangwu", "age":17}
]
#列表按照key = name 排序
stus.sort(key=lambda x: x['name'])
print(stus)
#列表按照key = age 排序
stus.sort(key=lambda x: x['age'])
print(stus)





